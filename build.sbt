name := """play-scala-intro"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  jdbc,
  "com.typesafe.play" %% "play-slick" % "2.0.3-BEN-SNAPSHOT",
  "mysql" % "mysql-connector-java" % "5.1.34",
  "org.activiti" % "activiti-engine" % "5.21.0",
  "org.springframework" % "spring-context" % "4.1.0.RELEASE",
  specs2 % Test
)
