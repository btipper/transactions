package dal

import java.util

import activiti.HodProcessEngineConfiguration
import com.google.inject.{Inject, Singleton}
import org.activiti.engine.task.Task
import org.activiti.engine.{ProcessEngine, ProcessEngines}
import slick.dbio.DBIOAction
import slick.dbio.Effect.{Read, Schema, Write}
import slick.jdbc.JdbcBackend
import slick.lifted.TableQuery
import slick.dbio.DBIO
import slick.driver.MySQLDriver.api._
import slick.profile.FixedSqlStreamingAction

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class DbExamples @Inject() (standaloneMySQLProcessEngineConfiguration: HodProcessEngineConfiguration) {

  def setUpBoth(db: JdbcBackend#DatabaseDef, suppliers: TableQuery[Suppliers], coffees: TableQuery[Coffees])(implicit ec: ExecutionContext): Future[String] = {
    val activitiFuture = setUpActivitiAsync(db)
    val slickFuture = setUpSlickAsync(db, suppliers, coffees)

    slickFuture.map(_ => "Activiti and Slick set up complete")
  }

  def setUpActivitiAsync(db: JdbcBackend#DatabaseDef)(implicit ec: ExecutionContext): Future[String] = {
    Future {
      //will be executed within transaction of ibatis
      val processEngine = standaloneMySQLProcessEngineConfiguration.pe
      deployProcessDefinition(processEngine)

      val runtimeService = processEngine.getRuntimeService
      val processInstance = runtimeService.startProcessInstanceByKey("my-process")
      val result = processEngine.getTaskService.createTaskQuery().singleResult()

      result.getName
    }
  }

  def setUpSlickAsync(db: JdbcBackend#DatabaseDef, suppliers: TableQuery[Suppliers], coffees: TableQuery[Coffees])(implicit ec: ExecutionContext): Future[String] = {
    val action = setUpSlickDBIOAction(suppliers, coffees)

    db.run(action).map(_ => "Slick set up complete")
  }

  def retrieveCoffeeAndSupplierNames(db: JdbcBackend#DatabaseDef, suppliers: TableQuery[Suppliers], coffees: TableQuery[Coffees]): Future[Seq[(String, String)]] = {
    val action = retrieveCoffeeAndSuppliersAction(suppliers, coffees)

    db.run(action)
  }

  def retrieveTaskNames(): mutable.Buffer[String] = {
    val processEngine = standaloneMySQLProcessEngineConfiguration.pe
    val runtimeService = processEngine.getRuntimeService
    val processInstance = runtimeService.startProcessInstanceByKey("my-process")
    val results: util.List[Task] = processEngine.getTaskService.createTaskQuery().list()

    results.map(_.getName)
  }

  private def retrieveCoffeeAndSuppliersAction(suppliers: TableQuery[Suppliers], coffees: TableQuery[Coffees]): FixedSqlStreamingAction[Seq[(String, String)], (String, String), Read] = {
    val coffeesWithSupplierNames = for {
      c <- coffees
      s <- suppliers if c.supID === s.id
    } yield (c, s.name)

    coffeesWithSupplierNames.filter(_._1.price > 8.00)
    .map(tuple => (tuple._1.name, tuple._2))
    .result
  }

  private def setUpSlickDBIOAction(suppliers: TableQuery[Suppliers], coffees: TableQuery[Coffees]): DBIOAction[Unit, NoStream, Write with Schema] = {
    DBIO.seq(
      (suppliers.schema ++ coffees.schema).create,
      suppliers += (101, "Acme, Inc.", "99 Market Street", "Groundsville", "CA", "95199"),
      suppliers += (49, "Superior Coffee", "1 Party Place", "Mendocino", "CA", "95460"),
      suppliers += (150, "The High Ground", "100 Coffee Lane", "Meadows", "CA", "93966"),
      coffees += ("Colombian", 101, 7.99, 0, 0),
      coffees += ("French_Roast", 49, 8.99, 0, 0),
      coffees += ("Espresso", 150, 9.99, 0, 0),
      coffees += ("Colombian_Decaf", 101, 8.99, 0, 0),
      coffees += ("French_Roast_Decaf", 49, 9.99, 0, 0)
    )
  }

  private def deployProcessDefinition(processEngine: ProcessEngine): Unit = {
    val repositoryService = processEngine.getRepositoryService
    repositoryService.createDeployment()
                    .addClasspathResource("VacationRequest.bpmn20.xml")
                    .deploy()

    println("Number of process definitions: " + repositoryService.createProcessDefinitionQuery().count())
  }
}
