package dal

import java.util.concurrent.{Executor, LinkedBlockingQueue, SynchronousQueue}
import java.util.concurrent.atomic.AtomicInteger

import slick.util.AsyncExecutor.{PrioritizedRunnable, _}
import slick.util.{AsyncExecutor, ManagedArrayBlockingQueue}

import scala.concurrent.ExecutionContext

object AsyncExecutorFactory {

  def createAsyncExecutor(queueSize: Int): AsyncExecutor = {
    new AsyncExecutor {
      // Before init: 0, during init: 1, after init: 2, during/after shutdown: 3
      private[this] val state = new AtomicInteger(0)

      @volatile private[this] var executor: Executor = _

      lazy val executionContext = {
        if(!state.compareAndSet(0, 1))
          throw new IllegalStateException("Cannot initialize ExecutionContext; AsyncExecutor already shut down")
        val queue = queueSize match {
          case 0 => new SynchronousQueue[Runnable]
          case -1 => new LinkedBlockingQueue[Runnable]
          case n =>
            new ManagedArrayBlockingQueue[Runnable](n * 2) {
              def accept(r: Runnable, size: Int) = r match {
                case pr: PrioritizedRunnable if pr.highPriority => true
                case _ => size < n
              }
            }
        }
        executor = createParentThreadExecutor()
        ExecutionContext.fromExecutor(executor, loggingReporter)
      }

      def close(): Unit = { }
    }
  }

  private def createParentThreadExecutor(): Executor = {
    new Executor {
      def execute(runnable: Runnable): Unit = runnable.run()     //all db operations should now be run on the request thread (no good for long-term - makes the entire thing synchronous rather than async)
    }
  }

}
