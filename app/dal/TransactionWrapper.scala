package dal

import com.google.inject.{Inject, Singleton}
import play.api.Logger
import slick.jdbc.{ConnectionWrapper, HodDataSource}

import scala.concurrent.{ExecutionContext, Future}

//TODO: BT: include rollback behaviour (will need to rollback in the event of a problem within either supplied function application <call to f()>
//or when trying to commit. Since these are executed in separate futures, it would be better if we could use a method of which
//defers recovery to the end of the futures chain (but we will need access to the connection object when recovering to rollback,
// so Try may not work). Also need to call clean up whether or not an exception is thrown
@Singleton
class TransactionWrapper @Inject() (dataSource: HodDataSource)  {

  //currently executing all transactional code on the supplied execution context (at the moment,
  //this simply uses the request thread, but will change in future)
  def asyncTaskInTransaction[A](f: () => Future[A])(implicit ec: ExecutionContext): Future[A] = {
    for {
      con <- retrieveConnectionAndSetAutoCommit()
      a   <- f()
      _   <- commit(con)
      _   <- cleanUp(con)                 //TODO: BT: this needs to be executed when an exception thrown as well (equivalent of finally, but across futures)
    } yield a
  }

  private def retrieveConnectionAndSetAutoCommit[A]()(implicit ec: ExecutionContext): Future[ConnectionWrapper] = {
    Future {
       dataSource.getConnection() match {
         case con: ConnectionWrapper =>
           con.setAutoCommitReal(false)
           con
         case _ => throw new RuntimeException("Unrecognised connectionFuture type. Should be a custom ConnectionWrapper.")
       }
    }
  }

  private def commit[A](con: ConnectionWrapper)(implicit ec: ExecutionContext): Future[Unit] = {
    Future(con.commitReal())
  }

  private def rollback(t: Throwable, connection: ConnectionWrapper)(implicit ec: ExecutionContext): Unit = {
    Logger.info("Exception thrown during transaction execution, rolling back transaction....", t)
    connection.rollback()
  }

  private def cleanUp(connection: ConnectionWrapper)(implicit ec: ExecutionContext): Future[Unit] = {
    Future {
      connection.closeRealConnection()
      HodDataSource.removeCurrentConnection()
    }
  }
}
