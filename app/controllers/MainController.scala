package controllers

import javax.inject._

import dal.AsyncExecutorFactory._
import dal.{TransactionWrapper, _}
import play.api.Logger
import play.api.i18n._
import play.api.mvc._
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.{ConnectionWrapper, HodDataSource, JdbcBackend}
import slick.lifted.TableQuery

import scala.concurrent.Future

class MainController @Inject()(hodDataSource: HodDataSource,
                               transactionWrapper: TransactionWrapper,
                               dbExamples: DbExamples,
                               val messagesApi: MessagesApi,
                               dataSource: HodDataSource) extends Controller with I18nSupport {

  private val db = Database.forDataSource(hodDataSource, createAsyncExecutor(queueSize = 3))
  private implicit val slickExecutionContext = db.ioExecutionContext

  private val suppliers = TableQuery[Suppliers]
  private val coffees = TableQuery[Coffees]

  def createActivitiSchema = Action.async {
    transactionWrapper.asyncTaskInTransaction(() => {
      val future = dbExamples.setUpActivitiAsync(db)
      future.map(result => Ok(views.html.index(result)))
    })
  }

  def createSlickSchema = Action.async {
    transactionWrapper.asyncTaskInTransaction(() => {
      val future = dbExamples.setUpSlickAsync(db, suppliers, coffees)
      future.map(result => Ok(views.html.index(result)))
    })
  }

  def createBothSchemas = Action.async {
    transactionWrapper.asyncTaskInTransaction(() => {
      val future = dbExamples.setUpBoth(db, suppliers, coffees)
      future.map(result => Ok(views.html.index(result)))
    })
  }

  def retrieveFromSlickAndActiviti = Action.async {
    transactionWrapper.asyncTaskInTransaction(() => {
      Future(retrieveTaskNamesString()).flatMap(_ => retrieveSlickNamesAsync(db)).map(names => Ok(views.html.index(names)))
    })
  }

  def retrieveTasks = Action.async {
    transactionWrapper.asyncTaskInTransaction(() => {
      val taskNamesFuture = Future(retrieveTaskNamesString())
      taskNamesFuture.map(taskNames => Ok(views.html.index(taskNames)))
    })
  }

  private def connectionWrapper(dataSource: HodDataSource): ConnectionWrapper = dataSource.getConnection match {
    case cw: ConnectionWrapper => cw
    case _ => throw new RuntimeException("Unrecognised connection type. Should be a custom ConnectionWrapper.")
  }

  private def retrieveTaskNamesString(): String = {
    val taskNames = dbExamples.retrieveTaskNames()
    taskNames.foreach(s => Logger.info("Task name: " + s))
    taskNames.mkString(", ")
  }

  private def retrieveSlickNamesAsync(db: JdbcBackend#DatabaseDef): Future[String] = {
    val future = dbExamples.retrieveCoffeeAndSupplierNames(db, suppliers, coffees)
    future.map(seq => seq.map(tuple => tuple._1 + ": " + tuple._2).mkString(","))
  }
}

