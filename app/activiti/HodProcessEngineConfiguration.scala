package activiti

import com.google.inject.{Provider, Singleton}
import org.activiti.engine.impl.cfg.StandaloneProcessEngineConfiguration
import org.activiti.engine.{ProcessEngine, ProcessEngineConfiguration}
import slick.jdbc.HodDataSource

@Singleton           //TODO: BT: note that per activiti docs "Standalone mode means that Activiti will manage the transactions on the JDBC connections that it creates. One transaction per service method."
class HodProcessEngineConfiguration extends StandaloneProcessEngineConfiguration with Provider[ProcessEngine] {

  this.dataSource = new HodDataSource
  this.databaseSchemaUpdate = ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE

  asyncExecutorEnabled = false           //TODO: BT: these were originally set to true; setting to false because we want to control the execution context activiti transactions execute in
  asyncExecutorActivate = false

  lazy val pe = buildProcessEngine()

  override def get(): ProcessEngine = {
    pe
  }
}
